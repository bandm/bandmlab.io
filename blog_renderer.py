"""
Renders the blog from data
"""

import os
import content_tree as ct
import parse_notebook as pn
import jinja2

from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import HtmlFormatter
import markdown

def render(content, settings, output_dir):
    """Given settings and content, writes the contents"""
    # start off with something super-simple, just a proof of concept
    # write each notebook to a directory, add an index that lists them all
    notebooks = content['notebooks']
    html_files = content['html_files']
    images = content['images']

    global_vars = make_global_vars(content)
    
    # render HTML files with just the general data
    for html_file in html_files:
        render_html(content, html_file, output_dir, global_vars)

    # render notebooks, one by one, too
    for notebook in notebooks:
        render_notebook(content, notebook, output_dir, global_vars)

    # save images
    for image in images:
        render_image(content, image, output_dir)


def get_jinja_env():
    return jinja2.Environment(
        loader = jinja2.FileSystemLoader('_layouts'))
        

def unique_path(node):
    """
    Makes a unique HTML file path and link for the given node. Returns
    relative to /
    """
    if isinstance(node, ct.HtmlFile):
        # make a path to HTML file
        return node.filename, node.filename
    elif isinstance(node, ct.NotebookData):
        # make a path to a notebook
        dir_name = 'notebooks/{}/'.format(node.notebook_dir)
        return dir_name + 'index.html', dir_name
    else:
        raise ValueError("Cannot make path for node '{}'".format(
            node))


def make_global_vars(content):
    """Makes 'global' variables like table of contents. Returns a dict"""
    result = {}
    # make table of contents
    all_notebooks = content['notebooks']

    toc = []
    for ii, notebook in enumerate(all_notebooks):
        notebook_url = unique_path(notebook)[1]
        notebook_title = notebook.content['metadata']['title']
        notebook_date = notebook.content['metadata']['date']
        toc.append((notebook_title, notebook_url, notebook_date))
    # sort, decreasing, by date
    toc.sort(key=lambda row: row[2], reverse=True)
    result['table_of_contents'] = toc
    result['title'] = notebook_title

    return result

    

def render_html(content, html_file, output_dir, global_vars):
    """Renders HTML, saves it to where it should go"""
    # for now just write out the HTML, no template rendering
    # save_path = os.path.join(output_dir, html_file.filename)
    save_path = os.path.join(output_dir, unique_path(html_file)[0])
    touch_file_dir(save_path)

    env = get_jinja_env()
    
    with open(save_path, 'w') as ff:
        template = env.from_string(html_file.contents)
        ff.write( template.render(**global_vars) )


def render_notebook(content, notebook, output_dir, global_vars):
    """Renders the notebook"""
    save_path = os.path.join(output_dir, unique_path(notebook)[0])
    touch_file_dir(save_path)

    # get the jinja environment
    env = get_jinja_env()
    
    # make the Python parser
    html_formatter = HtmlFormatter()
    css = html_formatter.get_style_defs('.highlight')
    python_lexer = PythonLexer()
    
    def python_pretty(python_code):
        return highlight(python_code, python_lexer, html_formatter)

    config = {'enable_dollar_delimiter': True}
    md = markdown.Markdown(extensions=['mdx_math'],
                           extension_configs={'mdx_math': config})
    def markdown_pretty(markdown_code):
        # return markdown.markdown(markdown_code)
        return md.convert(markdown_code)

    template = env.get_template('notebook.html')
    html = template.render(cells=notebook.content['cells'],
                           css = css,
                           name='Boom',
                           python_pretty = python_pretty,
                           markdown_pretty = markdown_pretty,
                           **global_vars
                           )

    with open(save_path, 'w') as ff:
        ff.write( html )


def render_image(content, image, output_dir):
    """Saves the image"""
    save_path = os.path.join(output_dir, 'notebooks', image[0])
    with open(save_path, 'wb') as ff:
        ff.write(image[1])

def touch_file_dir(path):
    """Touches the directory of the file"""
    dir_path = os.path.dirname(path)
    try:
        os.makedirs(dir_path)
    except OSError as ee:
        if ee.errno != 17:
            raise
