"""
Loads the notebooks and their config.
"""

import os
import collections

import parse_notebook as pn

def load_content(path):
    """Loads the full content of the page, returns it"""
    # load notebooks and their configs (null at the moment)
    # parse the notebooks
    # load html files from root directory, store their text
    notebook_dirs = os.listdir(os.path.join(path, '_notebooks'))
    lnotebooks = []
    for notebook_dir in notebook_dirs:
        # parse the notebook dir
        lnotebooks.append( parse_notebook_dir(
            os.path.join(path, '_notebooks'),
            notebook_dir) )

    # find all html files in root directory, pass them through
    lhtml_files = []
    for html_file in (path for path in os.listdir(path)
                      if path.endswith('.html')):
        lhtml_files.append( parse_html_file(path, html_file) )

    # find all images in the notebook directory
    limages = []
    os.chdir('_notebooks')
    for dirpath, dirs, files in os.walk('.'):
        for _file in files:
            if not _file.endswith('.jpg'):
                continue
            relpath = os.path.join(dirpath, _file)
            limages.append((relpath, open(relpath, 'rb').read()))
    os.chdir('..')

    return dict(
        notebooks = lnotebooks,
        html_files = lhtml_files,
        images = limages
        )

HtmlFile = collections.namedtuple('HtmlFile', ['filename', 'contents'])

def parse_html_file(root_dir, filename):
    """Returns a struct with the file's name and content. Short and sweet"""
    return HtmlFile(
        filename,
        open(os.path.join(root_dir, filename)).read())

NotebookData = collections.namedtuple('NotebookData',
                                      ['notebook_dir', 'content', 'config'])

def parse_notebook_dir(base_dir, notebook_dir):
    """Returns a struct with the notebook's name, parsed contents and post config"""
    # find the notebook in the dir
    notebook_files = [path for path in os.listdir(os.path.join(base_dir,
                                                               notebook_dir))
                      if path.endswith('.ipynb')]
    if len(notebook_files) != 1:
        raise RuntimeError("Directory '{}' contains {} notebooks (not 1)".format(
            notebook_dir, len(notebook_files)))
    # find the config - SKIP, that's for some other day
    # stick all the info into a struct and return
    notebook_content = pn.load_notebook(
        open(os.path.join(base_dir, notebook_dir, notebook_files[0])))

    result = NotebookData(notebook_dir, notebook_content, None)
    return result
