import json
import collections
import datetime
import re

def load_notebook(file_like):
    """Loads a JSON notebook"""
    file_loaded = json.load(file_like)
    Cell = collections.namedtuple('Cell', ['cell_type',
                                           'source',
                                           'outputs'])
    Output = collections.namedtuple('Output', ['output_type', 'content'])

    # extract header info (?)
    # get cells
    cells = file_loaded['cells']
    lcells = []
    
    for cell in cells:
        cell_type = cell['cell_type']
        source = '\n'.join(cell['source'])
        outputs = []
        for output in cell.get('outputs', []):
            # parse the output
            output_type_json = output['output_type']
            output_type = None
            content = None
            if output_type_json == 'stream':
                content = ''.join(output['text'])
                output_type = 'text'
            elif output_type_json in ('execute_result', 'display_data'):
                # is it a picture?
                if 'image/png' in output['data']:
                    content = output['data']['image/png'].replace('\n', '')
                    output_type = 'png'
                # is it html?
                elif 'text/html' in output['data']:
                    content = ''.join(output['data']['text/html'])
                    output_type = 'html'
                elif 'text/plain' in output['data']:
                    content = ''.join(output['data']['text/plain'])
                    output_type = 'text'
                else:
                    raise RuntimeError("Cannot parse output")
            elif output_type_json == 'error':
                raise RuntimeError(
                    "Will not parse error cell. Fix the error and try again.")
            else:
                raise RuntimeError("Cannot parse this cell")

            cell_output = Output(output_type, content)
            outputs.append(cell_output)
        cell_object = Cell(cell_type, source, outputs)
        lcells.append(cell_object)
    # get metadata
    # find first markdown cell, there find the first line like '# ...', make that
    title = None
    date = None
    try:
        first_markdown_cell = next((cell for cell in lcells
                               if cell.cell_type == 'markdown'))
        first_title_match = next(re.finditer(
            r'^# (.*)$',
            first_markdown_cell.source,
            flags=re.MULTILINE))
        title = first_title_match.group(1).strip()

        # a bit sub-optimal; we should check for date and title
        # separately - but that's for another day. If there is no
        # date, assume 'today' - e.g. a notebook under development
        date_str = next(re.finditer(
            r'Date: (\d{4}-\d{2}-\d{2})',
            first_markdown_cell.source)).group(1)
        date = datetime.datetime.strptime(date_str, '%Y-%m-%d')
    except StopIteration:
        title = '(no title)'
        date = datetime.datetime.today()
    # the title
    # if cannot find, mark it as (no title)
    metadata = dict(title=title, date=date)
    return dict(
        cells = lcells,
        metadata = metadata,
        )
