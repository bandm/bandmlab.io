#!/usr/bin/env python
"""Micro-server, serving from where it is told to"""

import os
import http.server
import socketserver
import argparse

PORT = 8765


def serve_from_dir(directory):
    # change to that dir
    os.chdir(directory)
    Handler = http.server.SimpleHTTPRequestHandler
    httpd = socketserver.TCPServer(("127.0.0.1", PORT), Handler)
    # Prevent 'cannot bind to address' errors on restart
    httpd.allow_reuse_address = True
    print("Serving at port", PORT)
    print("Ctrl+C to shut down")
    httpd.serve_forever()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('directory', default='public', nargs='?')
    args = parser.parse_args()

    serve_from_dir(args.directory)


if __name__ == '__main__':
    main()
