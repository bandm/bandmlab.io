# Usage: invoke build/serve/etc.
from invoke import task
import serve as serve_module

import blog_renderer
import content_tree

@task
def install(ctx):
    """Installs the virtual environment from requirements.txt"""
    ctx.run("virtualenv env")
    ctx.run("pip install -r requirements.txt")

@task
def freeze(ctx):
    """Freezes the requirements"""
    ctx.run("pip freeze > requirements.txt")

@task
def build(ctx):
    content = content_tree.load_content('.')
    blog_renderer.render(content, {}, 'public')
    
@task(build)
def serve(ctx):
    """Runs the server"""
    serve_module.serve_from_dir('public')
